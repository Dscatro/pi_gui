// Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
var connection = mysql.createConnection(config.db);

$(window).on('load', function(){
    
    var select1 = document.getElementById('inputGroupSelect01');
    var select2 = document.getElementById('inputGroupSelect02');
    var select3 = document.getElementById('inputGroupSelect03');

    $query = 'SELECT Username FROM operador WHERE Tipo=0;';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        text = JSON.stringify(rows);
        x = JSON.parse(text);
        for (i =0;i<x.length;i=i+1){
            select1.options[select1.options.length] = new Option(x[i].Username);
        }
        
    })
    $query = 'SELECT DISTINCT Numero_de_bancada FROM processo;';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        text = JSON.stringify(rows);
        x = JSON.parse(text);
        for (i =0;i<x.length;i=i+1){
            select2.options[select2.options.length] = new Option(x[i].Numero_de_bancada);
        }
        
    })
    $query = 'SELECT DISTINCT IDproduto FROM produto;';
    connection.query($query,(err, rows, fields) =>{
        console.log("IDProduto");
        if(err){
            return console.log("ERROR",err);
        }
        text = JSON.stringify(rows);
        x = JSON.parse(text);
        for (i =0;i<x.length;i=i+1){
            select3.options[select3.options.length] = new Option(x[i].IDproduto);
        }
        
    })
    
});

$( "#inputGroupSelect01 ").click(function(){
    var select1 = document.getElementById('inputGroupSelect01');
    var operadorInfo = select1.options[select1.selectedIndex].text;
    if(operadorInfo.toString() != "Escolha um operador"){
        $query = 'SELECT IDoperador FROM operador WHERE `Username` = "'+operadorInfo+'";';
            connection.query($query,(err, rows, fields) =>{
            if(err){
                return console.log("ERROR",err);
            }
            console.log(rows);
            text = JSON.stringify(rows[0]);//format json
            console.log(text);
            obj = JSON.parse(text); 
            console.log(obj.IDoperador);    
            sessionStorage.removeItem('localID');
            sessionStorage.setItem('localID',obj.IDoperador);        
        });
    }
})


$( "#searchBtn ").click(function(){
    var select1 = document.getElementById('inputGroupSelect01');
    var operadorInfo = select1.options[select1.selectedIndex].text;
    var select2 = document.getElementById('inputGroupSelect02');
    var bancadaInfo = select2.options[select2.selectedIndex].text;
    var select3 = document.getElementById('inputGroupSelect03');
    var produtoInfo = select3.options[select3.selectedIndex].text;
    var dataInfo = document.getElementById('formGroupExampleInput2').value;
    
    var where = "";
    var bancadaQuery = "";
    var prodQuery = "";
    var dataQuery = "";
    
    var query = "SELECT IDprocesso,Estado,Numero_de_pecas,IDproduto FROM processo ";
    if(operadorInfo.toString() != "Escolha um operador"){
        operadorInfo = sessionStorage.getItem('localID');
        where = 'WHERE `IDoperador` ="'+operadorInfo+'"';
    }
    if(bancadaInfo.toString() != "Escolha uma bancada"){
        if(where!=""){
            console.log("bancada working");
            bancadaQuery = 'AND `Numero_de_bancada` ="'+bancadaInfo+'"';
        }
        else{
            console.log("bancada working2");
            where ='WHERE `Numero_de_bancada` ="'+bancadaInfo+'"';
        }
    }
    if(produtoInfo.toString() != "Escolha um produto"){
        if(where!=""){
            console.log("produto working");
            prodQuery = 'AND `IDproduto` ="'+produtoInfo+'"';
        }
        else{
            console.log("produto working2");
            where ='WHERE `IDproduto` ="'+produtoInfo+'"';
        }
    }
    if(dataInfo.toString() != ""){
        if(where!=""){
            console.log("data working");
            dataQuery = 'AND `Data` ="'+dataInfo+'"';
        }
        else{
            console.log("data working2");
            where ='WHERE `Data` ="'+dataInfo+'"';
        }
    }
    final = ';'
    $query = query + where + bancadaQuery + prodQuery + dataQuery + final;
    console.log($query);
    var pro = [];
    var est = [];
    var pecas = [];
    var prod = [];
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        console.log(rows);
        for(i=0; i < rows.length; i++){
                text = JSON.stringify(rows[i]);//format json
                obj = JSON.parse(text);
                pro[i]=obj.IDprocesso;
                est[i]=obj.Estado; 
                pecas[i]=obj.Numero_de_pecas;
                prod[i]=obj.IDproduto;

        }
        sessionStorage.setItem('ProLen',pro.length);
        sessionStorage.setItem('Pro',pro);
        sessionStorage.setItem('Est',est); 
        sessionStorage.setItem('Pecas',pecas); 
        sessionStorage.setItem('Prod',prod);   
        sessionStorage.setItem('Page',1); 

    });
});

