// Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
var connection = mysql.createConnection(config.db);

window.onload=function(){
        var str = window.location.href;
        var Page = sessionStorage.getItem('Page')-1;
        var idValue = str.split('#');
        idValue = this.parseInt(idValue[1])
        Proc = Page * 10+idValue;
        var ProcessoArray = sessionStorage.getItem('Pro');
        var ProcessoSplit = ProcessoArray.split(',');
        var IDprocesso = ProcessoSplit[Proc];
        // connect to mysql
        connection.connect(function (err) {
        // in case of error
        if (err) {
            console.log(err.code);
            console.log(err.fatal);
        }
        // console.log("Connection succesfully established!");
        });
        //queries
        Query1(IDprocesso);
        setTimeout(function(){ 
            Query2();
            Query3();
        }, 150);{}
        
}

function Query1(IDprocesso){
    $query = 'SELECT * FROM processo WHERE `IDprocesso` ="'+IDprocesso+'";';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        console.log(rows);
        
        text = JSON.stringify(rows[0])//format json
        obj = JSON.parse(text)
            
        document.getElementById("IDProcesso").innerHTML = "Proc." + obj.IDprocesso;
        
        document.getElementById("estado").innerHTML = obj.Estado;
        if(obj.Estado == "A decorrer"){
            document.getElementById("estado").className = "badge badge-warning";
        }
        else if(obj.Estado == "Finalizado"){
            document.getElementById("estado").className = "badge badge-success";
        }
        else if(obj.Estado == "Cancelado"){
            document.getElementById("estado").className = "badge badge-danger";
        }
        

        var localDate = new Date(obj.Data);
        console.log(localDate);
        str1 = localDate.toJSON().slice(0, 11).replace('T', ' ');
        mysqlDate = str1.concat(localDate.toLocaleTimeString()); 
        console.log(mysqlDate);

        document.getElementById("Data").innerHTML = mysqlDate;

        document.getElementById("Bancada").innerHTML = obj.Numero_de_bancada;

        document.getElementById("TempoProcesso").innerHTML = obj.TempoProcesso+" segundos";

        document.getElementById("IDproduto").innerHTML = obj.IDproduto;

        document.getElementById("IDsuporte").innerHTML = obj.IDsuporte;

        document.getElementById("Numero_de_pecas").innerHTML = obj.Numero_de_pecas;
        sessionStorage.setItem('IDoperador',obj.IDoperador);
        sessionStorage.setItem('IDproduto',obj.IDproduto);
    });
}


function Query2(){
    var IDoperador = sessionStorage.getItem('IDoperador');
    this.console.log(IDoperador);
    $query = 'SELECT Username FROM `basedados`.`operador` WHERE `IDoperador` = "'+IDoperador+'";';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        text = JSON.stringify(rows[0])//format json
        // console.log(text);
        obj = JSON.parse(text)
        // console.log(obj);
        
        
        document.getElementById("Username").innerHTML = obj.Username;
        
    });
}
function Query3(){
    var IDproduto = sessionStorage.getItem('IDproduto');
    this.console.log(IDproduto);
    $query = 'SELECT Empresa,Pecas_iniciais FROM `basedados`.`produto` WHERE `IDproduto` = "'+IDproduto+'";';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        text = JSON.stringify(rows[0])//format json
        obj = JSON.parse(text)
        
        document.getElementById("Empresa").innerHTML = obj.Empresa;
        document.getElementById("Pecas_iniciais").innerHTML = obj.Pecas_iniciais;
        
    });
}

$( "#confirmBtn ").click(function(){
    var page = sessionStorage.getItem('Page');
 
    console.log(page);
    window.location.href ='results.html#'+page;
})