var fs = require('fs')


$( "#cancelBtn" ).click(function () {
    //Iniciar variaveis para fazer o acesso á base de dados
    var mysql = require('mysql');
    var config = require("../linkers/db-config.json");
    // Add the credentials to access your database
    var connection = mysql.createConnection(config.db);
    IDprocesso = sessionStorage.getItem('IDprocesso');
    var estado = "Cancelado";
    $query = 'UPDATE processo SET Estado="'+estado+'"WHERE IDprocesso ="'+IDprocesso+'";';
    connection.query($query, function (err, rows, fields) {
        if (err) {
            console.log("An error occurred performing the query.");
            console.log(err);
            return;
        }
        // console.log("Update Feito");
    })
    sessionStorage.removeItem('IDprocesso');
    sessionStorage.removeItem('ProcessInicialTime');
    sessionStorage.removeItem('log');
    sessionStorage.removeItem('NrPecas');
    window.location.href="operatorPage.html";
    
    
})

$( "#confirmBtn" ).click(function(){
    //Iniciar variaveis para fazer o acesso á base de dados
    var mysql = require('mysql');
    var config = require("../linkers/db-config.json");
    // Add the credentials to access your database
    var connection = mysql.createConnection(config.db);

    //Fazer as contas do tempo que demorou o processo
    const ProcessFinalTime = new Date();
    var ProcessInicialTime = new Date(sessionStorage.getItem('ProcessInicialTime'));
    var IDprocesso = sessionStorage.getItem('IDprocesso');    
    var estado = "Finalizado";
    var ProcessTime =  Math.floor((Date.parse(ProcessFinalTime)-Date.parse(ProcessInicialTime))/1000);
    var NrPecas = sessionStorage.getItem('NrPecas');
    // Guardar informação sobre o finalizar do processo
    $query = 'UPDATE processo SET Estado="'+estado+'", TempoProcesso="'+ProcessTime+'",\
     Numero_de_pecas = "'+NrPecas+'" WHERE IDprocesso ="'+IDprocesso+'";';
    connection.query($query, function (err, rows, fields) {
        if (err) {
            console.log("An error occurred performing the query.");
            console.log(err);
            return;
        }
        // console.log("Update Feito");
    })
    sessionStorage.removeItem('IDprocesso');
    sessionStorage.removeItem('ProcessInicialTime');
    sessionStorage.removeItem('log');
    sessionStorage.removeItem('NrPecas');
    window.location = "operatorPage.html"; // Redirecting to other page.
})


function fillInfo(info){
    $( "#productType" ).text(info[1])
    $( "#tabInfo" ).text(info[2])
    $( "#count" ).text(info[3])
}

function stampQR(info){
    $("#qrCode").attr("src", '../engine/computations/images/'+info[0]+'_QR.jpg')
}

$(window).on('load', function(){
    const urlParams = new URLSearchParams(window.location.search);
    const id = urlParams.get('id');

    var fileContent = fs.readFileSync(__dirname +'/../engine/computations/logs/'+id+'.txt', 'utf-8');
    fileContent = fileContent.split(',');;

    stampQR(fileContent);
    fillInfo(fileContent);
    getIDprocesso();
});



function getIDprocesso(){
//Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
// Add the credentials to access your database
var connection = mysql.createConnection(config.db);
$query1 = 'SELECT IDprocesso FROM processo ORDER BY IDprocesso DESC LIMIT 1;';
    connection.query($query1, function (err, rows, fields){
        if (err) {
            console.log("An error occurred performing the query.");
            console.log(err);
            return;
        }
        
        var x = JSON.stringify(rows);
        var IDprocesso = x.replace(/[^0-9]/g,''); 
        sessionStorage.setItem('IDprocesso',IDprocesso);
        console.log("ID="+IDprocesso);
    })
}