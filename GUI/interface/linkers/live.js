// Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
var connection = mysql.createConnection(config.db);


window.onload=function(){
    var IDoperador = [];
    var NBancada = [];
    var Estado = [];
    var Data = [];
    var User = [];
    var k = 0;
    $query ='SELECT IDoperador,Estado,IDprocesso, Numero_de_bancada, Data\
            FROM processo\
            WHERE Data IN(\
                SELECT MAX(Data)\
                FROM processo\
                GROUP BY Numero_de_bancada\
                ORDER BY IDprocesso DESC) ;';
    
    // 'SELECT * FROM processo  GROUP BY Numero_de_bancada WHERE ;';
    connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }
        
        for(i=rows.length-1; i >0 ; i--){
            text = JSON.stringify(rows[i]);
            obj = JSON.parse(text);
            // this.console.log(obj);
            
            // console.log(obj.Data[0]);
            IDoperador[k] = obj.IDoperador;
            NBancada[k] = obj.Numero_de_bancada;
            Estado[k] = obj.Estado;
            Data[k] = obj.Data
            // console.log(obj.IDprocesso);
            k++;
        }
        this.sessionStorage.setItem('LiveIDOperador',IDoperador);
        this.sessionStorage.setItem('LiveBancada',NBancada);
        this.sessionStorage.setItem('LiveEstado',Estado);
        this.sessionStorage.setItem('LiveData',Data);
    });

    setTimeout(function(){ 
        IDoperador = this.sessionStorage.getItem('LiveIDOperador');
        IDoperador = IDoperador.split(',');
        $query = 'SELECT Username, IDoperador FROM operador;'
        
        // 'SELECT * FROM processo  GROUP BY Numero_de_bancada WHERE ;';
        connection.query($query,(err, rows, fields) =>{
            if(err){
                return console.log("ERROR",err);
            }

            for(i=0; i < rows.length; i++){
                text = JSON.stringify(rows[i]);
                // this.console.log(text)
                x = JSON.parse(text);
                // this.console.log(x);
                for(j=0; j < IDoperador.length; j++){
                    // this.console.log("Session store "+IDoperador[j]+"\tAtual "+x.IDoperador);
                    if (IDoperador[j]==x.IDoperador){
                        // console.log("Entrou")
                        // Data[j]=x.Data;
                        User[j]=x.Username;
                    }
                }
            }
            // console.log(User);
        this.sessionStorage.setItem('LiveData',Data);
        this.sessionStorage.setItem('LiveUser',User);
    });
    setTimeout(function(){ 
        displayInfo();
    }, 100);{}


    }, 150);{}
    // 
    
    
  


}

function displayInfo() {
    User = this.sessionStorage.getItem('LiveUser');    
    User = User.split(',');
    NBancada = this.sessionStorage.getItem('LiveBancada');
    NBancada = NBancada.split(',');
    Estado = this.sessionStorage.getItem('LiveEstado');
    Estado = Estado.split(',');
    Data = this.sessionStorage.getItem('LiveData');
    Data = Data.split(',');

    var bancada ="Bancada";
    var user ="User";
    var data = "data";
    var estado ="Estado";
    var localDate;
    //console.log(endFor);
    for(i=0; i < 3; i++){
        // console.log(i);
        I = (i).toString();

        text1 = bancada.concat(I);
        document.getElementById(text1).innerHTML = ("Bancada "+NBancada[i]);

        text2 = user.concat(I);
        document.getElementById(text2).innerHTML = (User[i]);
        // console.log(User[i]+"\t"+text2);


        localDate = new Date(Data[i]);
        // console.log(localDate);
        str1 = localDate.toJSON().slice(0, 11).replace('T', ' ');
        mysqlDate = str1.concat(localDate.toLocaleTimeString());

        text3 = data.concat(I);
        document.getElementById(text3).innerHTML = (mysqlDate + " PT");
        
        
        text4 = estado.concat(I);        

        if(Estado[i] == "A decorrer"){
            document.getElementById(text4).className = "badge badge-warning";
            document.getElementById(text4).innerHTML = ("Em Funcionamento")
        }
        else if(Estado[i] == "Finalizado"){
            document.getElementById(text4).className = "badge badge-success";
            document.getElementById(text4).innerHTML = ("Finalizado")
        }
        else if(Estado[i] == "Cancelado"){
            document.getElementById(text4).className = "badge badge-danger";
            document.getElementById(text4).innerHTML = ("Cancelado")
        }
    }
}


$( "#confirmLiveBtn ").click(function(){
    sessionStorage.removeItem('LiveUser'); 
    sessionStorage.removeItem('LiveBancada'); 
    sessionStorage.removeItem('LiveData'); 
    sessionStorage.removeItem('LiveEstado'); 
    sessionStorage.removeItem('LiveIDOperador');  
})