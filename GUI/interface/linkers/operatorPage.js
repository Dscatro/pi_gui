console.log("opetator js is working...");
// Módulo para executar python
var pyshell = require('python-shell')
// Módulo para trabalhar com os ficheiros do sistema
var fs = require('fs')

// Atribuir um valor á bancada do operador Posteriormente isto será decido pelo gestor
var Bancada ="9"
//Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
// Add the credentials to access your database
var connection = mysql.createConnection(config.db);


$( "#scanBtn ").click(function(){
    pythonRun();   
    const ProcessInicialTime = new Date();
    setTimeout(function(){ 
        //Get log, ID of User and bancada do operador
        const log = sessionStorage.getItem('log');
        var IDUser = sessionStorage.getItem('IDUser');
        var bancada = sessionStorage.getItem('Bancada');
        // console.log(bancada);
        //Read the log file
        var info = fs.readFileSync(__dirname +'/../engine/computations/logs/'+log   +'.txt', {encoding:'utf8'});
        console.log(info);
        info = info.split(', ');        
        sessionStorage.setItem('NrPecas',info[3]);
        // Guardar tempo do inicio do processo
        sessionStorage.setItem('ProcessInicialTime',ProcessInicialTime);
        console.log(info[1]+' 1 ');

        str1 = ProcessInicialTime.toJSON().slice(0, 11).replace('T', ' ');
        mysqlDate1 = str1.concat(ProcessInicialTime.toLocaleTimeString()); 
        $query = 'INSERT INTO processo\
        (IDproduto,IDsuporte,IDoperador,Data,TempoProcesso,Numero_de_bancada)\
         VALUES ("' + info[1] + '", "' + info[2] + '","'+IDUser+'","'+mysqlDate1+'","0","'+bancada+'");';
      
        // Guardar na base de dados a informação obtida pelo scan
        connection.query($query, function (err, rows, fields){
            if (err) {
                console.log("An error occurred performing the qu    ery.");
                console.log(err);
                return;
            }
            console.log("Enviado!!");
        })
        // Buscar á base de dados o ID do processo
        
        
        // Mudar para a pagina scan.html
                url = 'scan.html?id=' + encodeURIComponent(log);
                console.log(url)
                document.location.href = url;
    }, 2000);
})

$( "#logoutBtn ").click(function(){
    // Fazer as contas do tempo que o utilizador passou em login
    const logoutTime = new Date();
    var loginTime = new Date(sessionStorage.getItem('loginTime'));
    var IDUser = sessionStorage.getItem('IDUser');
    // console.log("LogoutBtn\n"+logoutTime+"\n"+loginTime);
    var loggedTime =  Math.floor((Date.parse(logoutTime)-Date.parse(loginTime))/1000);
    str1 = loginTime.toJSON().slice(0, 11).replace('T', ' ');
    mysqlDate = str1.concat(loginTime.toLocaleTimeString());  
    // console.log(mysqlDate);
    // Guardar na BD o tempo que o utilizador esteve em login
    $query = 'UPDATE o1 SET TempoLogin="'+loggedTime+'" WHERE Data ="'+mysqlDate+'" AND IDoperador="'+IDUser+'"';
    connection.query($query, function (err, rows, fields) {
        if (err) {
            console.log("An error occurred performing the query.");
            console.log(err);
            return;
        }
        console.log("Update Feito");
    })
    sessionStorage.clear();  
})


function pythonRun(){
    let options = {
        mode: 'text',
        pythonPath: 'C:/Users/diogu/OneDrive - Universidade de Aveiro/Documents/Python/python.exe',
        pythonOptions: ['-u'], // get print results in real-time
        scriptPath: './engine/'
      };
    pyshell.PythonShell.run('computerVision.py', options, function(err, results){
        if (err) throw err;
        console.log(results)
        sessionStorage.setItem('log',results[0]);
        // url = 'scan.html?id=' + encodeURIComponent(results[0]);
        // console.log(url)
        // document.location.href = url;
    });
}


$(window).on('load', function(){
    // console.log("Name "+ sessionStorage.getItem('user'));
    $( "#employeName" ).text(sessionStorage.getItem('user'));
    $( "#IDoperator" ).text(sessionStorage.getItem('IDUser'));
    $( "#IDBancada" ).text("Bancada :"+Bancada);
    sessionStorage.setItem('Bancada',Bancada);
});