// Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
var connection = mysql.createConnection(config.db);

const Keyboard = {
    elements: {
        main: null,
        keysContainer: null,
        keys: []
    },

    eventHandlers: {
        oninput: null,
        onclose: null
    },

    properties: {
        value: "",
        capsLock: false
    },

    init() {
        // Create main elements
        this.elements.main = document.createElement("div");
        this.elements.keysContainer = document.createElement("div");

        // Setup main elements
        this.elements.main.classList.add("keyboard", "keyboard--hidden");
        this.elements.keysContainer.classList.add("keyboard__keys");
        this.elements.keysContainer.appendChild(this._createKeys());

        this.elements.keys = this.elements.keysContainer.querySelectorAll(".keyboard__key");

        // Add to DOM
        this.elements.main.appendChild(this.elements.keysContainer);
        document.body.appendChild(this.elements.main);

        // Automatically use keyboard for elements with .use-keyboard-input
        document.querySelectorAll(".use-keyboard-input").forEach(element => {
            element.addEventListener("focus", () => {
                this.open(element.value, currentValue => {
                    element.value = currentValue;
                });
            });
        });
    },

    _createKeys() {
        const fragment = document.createDocumentFragment();
        const keyLayout = [
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "backspace",
            "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "caps", "a", "s", "d", "f", "g", "h", "j", "k", "l",
            "done", "z", "x", "c", "v", "b", "n", "m", ",", ".", "?"
        ];

        // Creates HTML for an icon
        const createIconHTML = (icon_name) => {
            return `<i class="material-icons">${icon_name}</i>`;
        };

        keyLayout.forEach(key => {
            const keyElement = document.createElement("button");
            const insertLineBreak = ["backspace", "p", "l", "?"].indexOf(key) !== -1;

            // Add attributes/classes
            keyElement.setAttribute("type", "button");
            keyElement.classList.add("keyboard__key");

            switch (key) {
                case "backspace":
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHTML("backspace");

                    keyElement.addEventListener("click", () => {
                        this.properties.value = this.properties.value.substring(0, this.properties.value.length - 1);
                        this._triggerEvent("oninput");
                    });

                    break;

                case "caps":
                    keyElement.classList.add("keyboard__key--wide", "keyboard__key--activatable");
                    keyElement.innerHTML = createIconHTML("keyboard_capslock");

                    keyElement.addEventListener("click", () => {
                        this._toggleCapsLock();
                        keyElement.classList.toggle("keyboard__key--active", this.properties.capsLock);
                    });

                    break;

                case "enter":
                    keyElement.classList.add("keyboard__key--wide");
                    keyElement.innerHTML = createIconHTML("keyboard_return");

                    keyElement.addEventListener("click", () => {
                        this.properties.value += "\n";
                        this._triggerEvent("oninput");
                    });

                    break;

                case "space":
                    keyElement.classList.add("keyboard__key--extra-wide");
                    keyElement.innerHTML = createIconHTML("space_bar");

                    keyElement.addEventListener("click", () => {
                        this.properties.value += " ";
                        this._triggerEvent("oninput");
                    });

                    break;

                case "done":
                    keyElement.classList.add("keyboard__key--wide", "keyboard__key--dark");
                    keyElement.innerHTML = createIconHTML("check_circle");

                    keyElement.addEventListener("click", () => {
                        this.close();
                        this._triggerEvent("onclose");
                    });

                    break;

                default:
                    keyElement.textContent = key.toLowerCase();

                    keyElement.addEventListener("click", () => {
                        this.properties.value += this.properties.capsLock ? key.toUpperCase() : key.toLowerCase();
                        this._triggerEvent("oninput");
                    });

                    break;
            }

            fragment.appendChild(keyElement);

            if (insertLineBreak) {
                fragment.appendChild(document.createElement("br"));
            }
        });

        return fragment;
    },

    _triggerEvent(handlerName) {
        if (typeof this.eventHandlers[handlerName] == "function") {
            this.eventHandlers[handlerName](this.properties.value);
        }
    },

    _toggleCapsLock() {
        this.properties.capsLock = !this.properties.capsLock;

        for (const key of this.elements.keys) {
            if (key.childElementCount === 0) {
                key.textContent = this.properties.capsLock ? key.textContent.toUpperCase() : key.textContent.toLowerCase();
            }
        }
    },

    open(initialValue, oninput, onclose) {
        this.properties.value = initialValue || "";
        this.eventHandlers.oninput = oninput;
        this.eventHandlers.onclose = onclose;
        this.elements.main.classList.remove("keyboard--hidden");
    },

    close() {
        this.properties.value = "";
        this.eventHandlers.oninput = oninput;
        this.eventHandlers.onclose = onclose;
        this.elements.main.classList.add("keyboard--hidden");
    }
};

window.addEventListener("DOMContentLoaded", function () {
    Keyboard.init();
});


window.onload=function(){
    document.getElementById("btn").addEventListener("click", () => {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;
        // connect to mysql
        connection.connect(function (err) {
        // in case of error
        if (err) {
            console.log(err.code);
            console.log(err.fatal);
        }
        // console.log("Connection succesfully established!");
        });
        //queries
        $query = 'SELECT * FROM operador;';
        connection.query($query,(err, rows, fields) =>{
        if(err){
            return console.log("ERROR",err);
        }

        //console.log(len)
        for(i=0; i < rows.length; i++){
            text = JSON.stringify(rows[i])//format json
            obj = JSON.parse(text)
            ID = parseInt(obj.IDoperador)
            if(username == obj.Username  && password == obj.Password ){
                //Save user
                if(!obj.Tipo){
                    saveDataOpe(ID,obj.Username)
                    window.location = "operatorPage.html"; // Redirecting to other page.
                }
                else{
                    saveDataGes(ID,obj.Username)
                    window.location = "admin.html"; // Redirecting to other page.
                }
                return
            }
        }
        // Caso a password esteja errada e os valores não estejam vazios \
        // Imprimir mensagem de erro
        if(username != '' && password != ''){
            var phrase = 'Username/Password errada';
            document.getElementById("frase").innerHTML = phrase;
           }
           document.getElementById("username").value = '';
           document.getElementById("password").value = '';
        });

    }, false);

  }

function saveDataOpe(ID,User) {
    // Obter o tempo em que o utilizador deu o login e formata-lo para MySql
    const loginTime = new Date();
    const mysqlDate = loginTime.toISOString().slice(0, 19).replace('T', ' ');
    // Guardar dados no PC para utilização futura
    if (typeof(Storage) !== "undefined") {
        sessionStorage.setItem('user',User);
        sessionStorage.setItem('IDUser',ID);
        sessionStorage.setItem('loginTime',loginTime);
    } else {
        alert('session storage not supported');
    } 
    
    // Enviar para a base de dados a informação de quando o utilizador deu login
    $query = 'INSERT INTO o1(IDoperador,Username,Data,TempoLogin ) VALUES ( "' + ID +'","' + User+'","' + mysqlDate+'","0");';
    connection.query($query, function (err, rows, fields) {
        if (err) {
            console.log("An error occurred performing the query.");
            console.log(err);
            return;
        }
    })
}


function saveDataGes(ID,User) {
    // Guardar dados no PC para utilização futura
    if (typeof(Storage) !== "undefined") {
        sessionStorage.setItem('user',User);
        sessionStorage.setItem('IDUser',ID);
    } else {
        alert('session storage not supported');
    } 
}