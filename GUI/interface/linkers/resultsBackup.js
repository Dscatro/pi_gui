// Iniciar variaveis para fazer o acesso á base de dados
var mysql = require('mysql');
var config = require("../linkers/db-config.json");
var connection = mysql.createConnection(config.db);

window.onload=function(){
        var ProLen = sessionStorage.getItem('ProLen');
        var page = Math.ceil(ProLen/10);
        sessionStorage.setItem('LastPage',page);
        //document.getElementById("last").innerHTML=page;

        // dar valores de paginaçao
        var text = "index";
        for(i=1; i<6; i++){
            temp = (i).toString();
            text = text.concat(temp);
            document.getElementById(text).innerHTML=i;
            text="index";
        }
        // "display: none"
        for(i=5; i>5; i--){
            temp = (i).toString();
            text = text.concat(temp);
            document.getElementById(text).innerHTML=i;
            document.getElementById(text).style = "display: none";
            text="index";
        }
        // active da pagina selecionada
        var str = window.location.href;
        var idValue = str.split('#');
        var page = idValue[1];
        sessionStorage.setItem('Page',page);
        temp = (page).toString();
        text = text.concat(temp);
        document.getElementById(text).className = "active";


        var table = document.getElementById("myTable");
        // connect to mysql
        connection.connect(function (err) {
        // in case of error
        if (err) {
            console.log(err.code);
            console.log(err.fatal);
        }
        // console.log("Connection succesfully established!");
        });
        //queries        
        

        var PecasArray = sessionStorage.getItem('Pecas');
        var PecasSplit = PecasArray.split(',');

        var ProdArray = sessionStorage.getItem('Prod');
        var ProdSplit = ProdArray.split(',');

        var arrProd= [];
        var arrPecas= [];
        var PecasPerdidas = [];
        $query = 'SELECT IDproduto,Pecas_iniciais FROM `basedados`.`produto`;';
        connection.query($query,(err, rows, fields) =>{
            if(err){
                return console.log("ERROR",err);
            }
            
            for(i=0; i < rows.length; i++){
                text = JSON.stringify(rows[i])//format json
                // console.log(text);
                obj = JSON.parse(text)
                //criar arrays
                arrProd[i]=obj.IDproduto;
                //this.console.log(obj.IDproduto);
                arrPecas[i]=obj.Pecas_iniciais;
                //this.console.log(obj.Pecas_iniciais);
            }
            //array feito
            //this.console.log(arrProd);
            //this.console.log(arrPecas);
            var soma=0;
            var total=0;
            for(i=0; i < ProdSplit.length; i++){
                for(j=0; j < arrProd.length; j++){
                    if(ProdSplit[i]==arrProd[j]){
                        PecasPerdidas[i]=arrPecas[j]-PecasSplit[i];
                        soma=soma+PecasPerdidas[i];
                        total=arrPecas[j]+total;
                    }
                }
            }

            //this.console.log(PecasPerdidas);
            var percentagem = ((total-soma)/total)*100;
            document.getElementById("p2").innerHTML = Math.round(percentagem)+"%";
            document.getElementById("p1").innerHTML = soma;
            sessionStorage.setItem('PecasPerdidas',PecasPerdidas);
            Table();
        
        });
  }

  function Table(){
    var str = window.location.href;
    var idValue = str.split('#');
    var page = idValue[1]-1;

    var ProcessoArray = sessionStorage.getItem('Pro');
    var ProcessoSplit = ProcessoArray.split(',');
    //this.console.log(ProcessoSplit);
    var len = ProcessoSplit.length;
    //this.console.log(len);
    document.getElementById("resultados").innerHTML = len;

    var EstadoArray = sessionStorage.getItem('Est');
    var EstadoSplit = EstadoArray.split(',');
    

    var PecasPerdidasArray = sessionStorage.getItem('PecasPerdidas');
    //this.console.log(PecasPerdidasArray);
    var PecasPerdidasSplit = PecasPerdidasArray.split(',');

    var text="id";
    var estado="estado";
    var pecas ="pecas";
    var temp = ProcessoSplit.length - (10*page);
    //console.log(endFor);
    if(temp < 10){
        endFor = temp;
    }
    else{
        endFor = 10;
    }
    for(i=0; i < endFor; i++){
        console.log(i);
        text1 = (i).toString();
        text = text.concat(text1);
        var IDprocesso = ProcessoSplit[i+(page*10)];
        //console.log("ID",IDprocesso,"text: ",text);
        document.getElementById(text).innerHTML = IDprocesso;
        
        pecas1 = (i).toString();
        var pecas = pecas.concat(pecas1);
        var Pecas = PecasPerdidasSplit[i+(page*10)];
        //console.log("Pecas",Pecas,"text: ",pecas);
        document.getElementById(pecas).innerHTML = Pecas;

        estado1 = (i).toString();
        var estado = estado.concat(estado1);
        var Estado = EstadoSplit[i+(page*10)];
        //console.log("Estado ",Estado,"text: ",estado);
        document.getElementById(estado).innerHTML = Estado;
       
        
        if(Estado == "A decorrer"){
            document.getElementById(estado).className = "badge badge-warning";
        }
        else if(Estado == "Finalizado"){
            document.getElementById(estado).className = "badge badge-success";
        }
        else if(Estado == "Cancelado"){
            document.getElementById(estado).className = "badge badge-danger";
        }
        text="id";
        estado="estado";
        pecas ="pecas";
    }
  }

  $( "#last").click(function(){
    var page = sessionStorage.getItem('LastPage');

    window.location='results.html#'+page;
    window.location.reload();
    console.log(windows.location);
    
})

  $( "#newSearchBtn").click(function(){
    sessionStorage.removeItem('localID');   
    sessionStorage.removeItem('Bancada');   
    sessionStorage.removeItem('IDproduto'); 
    sessionStorage.removeItem('PecasPerdidas');       
    sessionStorage.removeItem('Pro');
    sessionStorage.removeItem('Est'); 
    sessionStorage.removeItem('Pecas'); 
    sessionStorage.removeItem('Prod');
    sessionStorage.removeItem('Page');  
    
})

  $( "#backBtn").click(function(){
    sessionStorage.removeItem('localID');   
    sessionStorage.removeItem('Bancada');   
    sessionStorage.removeItem('IDproduto');
    sessionStorage.removeItem('PecasPerdidas');       
    sessionStorage.removeItem('Pro');
    sessionStorage.removeItem('Est'); 
    sessionStorage.removeItem('Pecas'); 
    sessionStorage.removeItem('Prod');  
    sessionStorage.removeItem('Page'); 
})