    function registerUser() {
      var username = document.getElementById("username").value;
      var password = document.getElementById("password").value;
        
      if (username == '' || password == '') {
            displayNotification('Error!', 'Values cannot be empty');
            return
      }

        var mysql = require('mysql');
        var config = require("./db-config");

        // Add the credentials to access your database
        var connection = mysql.createConnection(config.db);

        // connect to mysql
        connection.connect(function (err) {
            // in case of error
            if (err) {
                console.log(err.code);
                console.log(err.fatal);
            }
        });

        // Perform a query

        $query = 'INSERT INTO operador(Username, Password) VALUES ("' + username + '", "' + password + '");';

        connection.query($query, function (err, rows, fields) {
            if (err) {
                console.log("An error occurred performing the query.");
                console.log(err);
                return;
            }

            console.log("Query successfully executed");
        });

        // Close the connection
        connection.end(function () {
            // The connection has been closed
        });

        // display notification
        displayNotification('Done!', 'New user registered successfully');
    }

    function displayNotification(titleValue, notificationValue) {
        const notification = {
            title: titleValue,
            body: notificationValue
        }

        const myNotification = new window.Notification(notification.title, notification)
    }