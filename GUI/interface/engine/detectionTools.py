import time
import cv2
import numpy as np
import detectionTools
import pyzbar.pyzbar as pyzbar

# Needed global
productType = ''
tabInfo = ''
contagem = 0

codeCoordinatesX = 0
codeCoordinatesY = 0
codeCoordinatesW = 0
codeCoordinatesH = 0

def getImageName():
    timeStr = time.strftime("%d%m%Y_%H%M%S")

    return timeStr

def getCurrentTime():
    localTime = time.asctime( time.localtime(time.time()) )

    return localTime

def putTimestamp(image, timestamp):
    height,_,_ = image.shape
    cv2.putText(image, timestamp, (0,height-10), cv2.FONT_HERSHEY_PLAIN, 2, (0,0,0), 2)

    return image

def extractQR(image):
    decodedObjects = pyzbar.decode(image)
    
    for obj in decodedObjects:
        (x, y, w, h) = obj.rect
    
    cropQR = image[y:y+h, x:x+w]

    return cropQR

def getProductInfo(image):
    global codeCoordinatesX, codeCoordinatesY, codeCoordinatesW, codeCoordinatesH, productType, tabInfo

    decodedObjects=pyzbar.decode(image)

    for obj in decodedObjects:
        (x, y ,w ,h)=obj.rect
        codeCoordinatesX = x
        codeCoordinatesY = y
        codeCoordinatesW = w
        codeCoordinatesH = h

        cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),4)

        objData=obj.data.decode("utf-8")

        objData=objData.split(",")

        cv2.putText(image, "ID : " + objData[0], (x-50,y+h+50),cv2.FONT_HERSHEY_PLAIN, 2, (0,0,0), 2)
        cv2.putText(image, "Tab. no. : " + objData[1], (x-50,y+h+100),cv2.FONT_HERSHEY_PLAIN, 2, (0,0,0), 2)
        
        productType = objData[0]
        tabInfo = objData[1]
    
    return image

def scanProducts(image, mask, imageToStamp):
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    height, width, _ = image.shape
    global contagem

    totalArea = 0
    numberOfProducts = 0

    for cnt in contours:
        totalArea = totalArea + cv2.contourArea(cnt)
    
    meanArea = totalArea/len(contours)

    for cnt in contours:
        if (cv2.contourArea(cnt) >= meanArea):
            cv2.drawContours(image, cnt,-1,(0,255,0),2)
            numberOfProducts += 1

    contagem = numberOfProducts

    return image


def cutFrame(image):
    global codeCoordinatesX, codeCoordinatesY, codeCoordinatesW, codeCoordinatesH
    
    height, width, _ = image.shape

    croppedImage = image[0:int(height/3), 0:codeCoordinatesX]

    return croppedImage


def getInfoCoordinates(image):
    decodedObjects=pyzbar.decode(image)

    for obj in decodedObjects:
        (x, y ,w ,h)=obj.rect
        return (x, y, w, h)

    return (100,100,0,0)

def stampCountingNumber(image):
    global codeCoordinatesX, codeCoordinatesY, codeCoordinatesW, codeCoordinatesH

    textCoordinates = (codeCoordinatesX-50, codeCoordinatesY+codeCoordinatesH+150)
    cv2.putText(image, "Contagem : " + str(contagem), textCoordinates, cv2.FONT_HERSHEY_PLAIN, 2, (0,0,0), 2)
    
    return image

def getCount():
    return str(contagem)

def getProductType():
    return str(productType)

def getTabInfo():
    return str(tabInfo)

def generateLabeledFile(fileName):
    file = open('C:/Users/diogu/OneDrive - Universidade de Aveiro/universidade/4_ano/PI/GUI/UI_v2/GUI/interface/engine/computations/logs/'+fileName+'.txt', "w+")

    file.write("{}, {}, {}, {}".format(fileName, getProductType(), getTabInfo(), getCount()))

    file.close()