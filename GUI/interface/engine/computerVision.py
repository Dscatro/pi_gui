import cv2
import numpy as np
import pyzbar.pyzbar as pyzbar
from detectionTools import *
import sys
import os
import json

# Set running variables from configs.json file

# This variable can be set to 1 if user wants to see debug info while program runs
DEBUG_MODE = 1

# Store image name to use
imageName = getImageName()
print(imageName)

# Read image
originalFrame = cv2.imread('C:/Users/diogu/OneDrive - Universidade de Aveiro/universidade/4_ano/PI/GUI/UI_v2/GUI/interface/engine/Images/teste3.jpg')

# Timestamp
timestampImage = putTimestamp(originalFrame, str(getCurrentTime()))
if(DEBUG_MODE):
    print("[1/7] - Timestamp created...")

# QR cut to use front-end
qrCut = extractQR(originalFrame)
cv2.imwrite('C:/Users/diogu/OneDrive - Universidade de Aveiro/universidade/4_ano/PI/GUI/UI_v2/GUI/interface/engine/computations/images/'+imageName+'_QR.jpg', qrCut)
print("[QR] - ./computations/images/{}_QR.jpg".format(imageName))

# QR Detection
qrImage = getProductInfo(timestampImage)
if(DEBUG_MODE):
    print("[2/7] - QR info created...")

# Crop image for analysis
croppedImage = cutFrame(originalFrame)
if(DEBUG_MODE):
    print("[3/7] - Crop created...")

# Create gray scale version of original frame
grayFrame = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
if(DEBUG_MODE):
    print("[4/7] - Gray scale generated...")

# Blurred frame
blurredFrame = cv2.GaussianBlur(grayFrame, (7, 7), 1)
if(DEBUG_MODE):
    print("[5/7] - Blurred applied...")

# Threshold frame
ret,thresholdFrame = cv2.threshold(blurredFrame,220,255,cv2.THRESH_BINARY)
if(DEBUG_MODE):
    print("[6/7] - Mask created...")

# Draw contours
productContours = scanProducts(croppedImage, thresholdFrame, qrImage)
if(DEBUG_MODE):
    print("[6/7] - Contours created...")

# Save final image
finishedImage = stampCountingNumber(timestampImage)
cv2.imwrite('C:/Users/diogu/OneDrive - Universidade de Aveiro/universidade/4_ano/PI/GUI/GUI_04_20/GUI/interface/engine/computations/images/'+imageName+'_final.jpg', finishedImage)
print("[FINAL IMG] - ./computations/images/{}_final.jpg".format(imageName))

# Generate a file that supports iteration
generateLabeledFile(imageName)
print("[LOG] - ./computations/logs/{}.txt".format(imageName))

cv2.destroyAllWindows()

sys.stdout.flush()